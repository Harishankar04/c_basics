﻿using System.Diagnostics.Contracts;

namespace PostCourseActivity
{
    public class Program
    {
        public static void PrintEvenNumbers()
        {
            Console.WriteLine("All the even numbers in between 50 and 200");
            for(int i=50;i<=200;i++)
            {
                if (i % 2 == 0)
                    Console.WriteLine(i);
            }
        }
        public static void SortArray()
        {
            Console.WriteLine("The program for Sorting the Array");
            Console.WriteLine("Enter the array size: ");
            int size = Convert.ToInt32(Console.ReadLine());
            int[] numArray = new int[size];

            Console.WriteLine("The array value : ");
            for(int i=0;i<size;i++)
            {
                numArray[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine("The array before sorting will be : ");
            for (int i = 0; i < size; i++)
            { 
                Console.Write(numArray[i]+"\t"); 
            }
            Array.Sort(numArray);
            Console.WriteLine("\nThe array after sorting will be  : ");
            foreach(int i in numArray)
            {
                Console.Write(i+"\t");
            }
        }
        enum Days
        {
            Monday,
            Tuesday,
            Wednesday,
            Thursday,
            Friday,
            Saturday,
            Sunday
        }
        public static void EnumExe()
        {
            Console.WriteLine("The program to print Wednesday from enum values using Switch case");
            Days theday = Days.Wednesday;
            switch(theday)
            {
                case Days.Monday:
                    Console.WriteLine(Days.Monday);
                    break;
                case Days.Tuesday:
                    Console.WriteLine("Tuesday");
                    break;
                case Days.Wednesday:
                    Console.WriteLine(theday);
                    break;
                case Days.Thursday:
                    Console.WriteLine(Days.Thursday);
                    break;
                case Days.Friday:
                    Console.WriteLine(Days.Friday);
                    break;
                case Days.Saturday:
                    Console.WriteLine(Days.Saturday);
                    break;
                case Days.Sunday:
                    Console.WriteLine(Days.Sunday);
                    break;
                default:
                    Console.WriteLine("Nothing to display");
                    break;
            }
            Console.WriteLine("Switch case executed");
        }
        public static int AddIntegers(int num1,int num2)
        {
            return num1 + num2;
        }
        public static int AddIntegers(int num1,int num2,int num3)
        {
            return num1 + num2 + num3;
        }
        public static int AddIntegers(int num1,int num2,int num3,int num4)
        {
            return num1 + num2 + num3 + num4;
        }
        public static void OverloadingConcept()
        {
            Console.WriteLine("Calling the AddIntegers with just 2 values to add and the answer is : ");
            Console.WriteLine(AddIntegers(10, 20));
            Console.WriteLine("Calling the AddIntegers with 3 Parameters and the output will be : ");
            Console.WriteLine(AddIntegers(15, 15, 15));
            Console.WriteLine("Calling the AddInteger function with 4 Parameters then the output will be : ");
            Console.WriteLine(AddIntegers(10, 10, 10, 10));
        }
        public static void Main(string[] args)
        {
            int option;
            char ans = 'y';
            do
            {
                Console.WriteLine("<---Assignment Project--->\n1.To Print All The Even Numbers From 50 to 200.\n2.To Create an Array of Different Values and Print them." +
                "\n3.To Print Wednesday from a group of Enum Values using switch cases.\n4.To define 3 Add Integer Methods with same Name and with different parameters" +
                " using Method OverLoading Concept.\n5.Inheritance Concept.");
                Console.WriteLine("Enter any option from the menu to execute it :-");
                option = Convert.ToInt32(Console.ReadLine());
                switch (option)
                {
                    case 1:
                        PrintEvenNumbers();
                        break;
                    case 2:
                        SortArray();
                        break;
                    case 3:
                        EnumExe();
                        break;
                    case 4:
                        OverloadingConcept();
                        break;
                    case 5:
                        ChildClass CC = new ChildClass();
                        CC.val1 = 100;
                        CC.SampleMethod();
                        CC.ChildMethod();
                        break;
                    default:
                        Console.WriteLine("Invalid Entry, Sorry!");
                        break;
                }
                Console.WriteLine("\nDo you want to continue(Enter \'y\' for Yes or \'n\' for No : ");
                ans = Convert.ToChar(Console.ReadLine());
            } 
            while (ans == 'y' || ans=='Y');
        }
    }
    public class ParentClass
    {
        public int val1;
        public void SampleMethod()
        {
            Console.WriteLine("The value received after the creation of the child class object after Inheritance : "+val1);
            Console.WriteLine("I am from the Parent class being invoked by the child class");
        }
    }
    public class ChildClass : ParentClass
    {
        public void ChildMethod()
        {
            Console.WriteLine("I am a Child Class method");
        }
    }
}
